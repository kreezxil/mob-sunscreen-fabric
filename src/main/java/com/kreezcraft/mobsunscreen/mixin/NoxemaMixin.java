package com.kreezcraft.mobsunscreen.mixin;

import net.minecraft.client.gui.screen.TitleScreen;

import com.kreezcraft.mobsunscreen.MobSunscreen;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(TitleScreen.class)
public class NoxemaMixin
{
	@Inject(at = @At("HEAD"), method = "init()V")
	private void init(CallbackInfo info) {
		MobSunscreen.LOGGER.info("This line is printed by an example mod mixin!");
	}
}
